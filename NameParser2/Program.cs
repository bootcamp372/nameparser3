﻿using System;
using System.Security.Principal;

namespace NameParser
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a name:");
            string name = Console.ReadLine();
            string answer = "y";

            do
            {
                string firstName = ParseName(ref name, "firstName");
                string midName = ParseName(ref name, "midName");
                string lastName = ParseName(ref name, "lastName");

                Console.WriteLine($"First Name: {firstName}");
                Console.WriteLine($"Middle Name: {midName}");
                Console.WriteLine($"Last Name: {lastName}");
                Console.WriteLine("Enter another name? y/n");
                try
                {
                    answer = Console.ReadLine().ToUpper();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid Input. Error:" + e);
                    return;
                }
                if (answer != "N" && answer != "Y")
                {
                    Console.WriteLine("Enter Y or N");
                    answer = Console.ReadLine().ToUpper();
                }
                if (answer == "N")
                {
                    break;
                }
            } while (answer == "y");
        }

        static string ParseName(ref string name, string nameType)
        {

            int pos = name.IndexOf(" ");
            int pos2;
            string nameOut = "";

            switch (nameType)

            {
                case "firstName":
                    nameOut = name.Substring(0, pos);
                    break;
                case "midName":
                    pos = name.IndexOf(" ") - 1;
                    pos2 = name.LastIndexOf(" ");
                    nameOut = name.Substring(pos + 1, pos2 - pos);
                    break;
                case "lastName":
                    nameOut = name.Substring(pos + 1);
                    break;
                default:
                    nameOut = "";
                    Console.WriteLine("**Error: unrecognized major code");
                    break;
            }

            return nameOut;
        }
    }


}